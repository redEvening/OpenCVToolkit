package de.RedEvening.OpenCVUtil;

import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class Controller{

    @FXML
    private MenuItem closeMenuItem = new MenuItem();
    @FXML
    private MenuItem detectFacesMenuItem = new MenuItem();
    @FXML
    private MenuItem swapFacesMenuItem = new MenuItem();
    @FXML
    private ImageView imageView = new ImageView();
    @FXML
    private ViewModel viewModel = new ViewModel();
    private Window mainWindow;

    public void openFile() throws IOException {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(mainWindow);

        if(file == null){
            return;
        }

        // BufferedImage is necessary to check whether file is an image of not
        BufferedImage buffer = ImageIO.read(file);

        if(buffer == null){
            return;
        }

        Image image = SwingFXUtils.toFXImage(buffer, null);
            viewModel.setCurrentFile(file);
            viewModel.getCurrentImgProperty().set(image);
    }

    public void closeFile(){
        viewModel.getCurrentImgProperty().set(null);
        viewModel.setCurrentFile(null);
    }

    public void detectFaces() throws IOException{
        Image newImage = DetectFacesUtil.detectFaces(viewModel.getCurrentFile());
        viewModel.getCurrentImgProperty().set(newImage);
    }

    public void swapFaces() throws  IOException{
        Image newImage = ShuffleFacesUtil.shuffleFaces(viewModel.getCurrentFile());
        viewModel.getCurrentImgProperty().set(newImage);
    }

    public void exit(){
        Platform.exit();
    }

    void setupBindings(Window w){
        mainWindow = w;
        imageView.fitWidthProperty().bind(mainWindow.widthProperty());
        imageView.fitHeightProperty().bind(mainWindow.heightProperty());

        //Menu Items
        BooleanBinding fileNotOpened = viewModel.getFileIsOpenedProperty().not();
        closeMenuItem.disableProperty().bind(fileNotOpened);
        closeMenuItem.disableProperty().bind(fileNotOpened);
        detectFacesMenuItem.disableProperty().bind(fileNotOpened);
        swapFacesMenuItem.disableProperty().bind(fileNotOpened);

        //Image View
        imageView.imageProperty().bind(viewModel.getCurrentImgProperty());
    }
}
