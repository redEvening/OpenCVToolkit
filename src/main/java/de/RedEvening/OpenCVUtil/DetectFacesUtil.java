package de.RedEvening.OpenCVUtil;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.imgcodecs.Imgcodecs;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

final class DetectFacesUtil {

    private DetectFacesUtil(){ }

    static{
        // .subString is needed as Path is appended with "/", "/C:/..."
        String path = DetectFacesUtil.class.getResource(
                "/lbpcascade_frontalface.xml").getPath().substring(1);
        faceDetector = new CascadeClassifier(path);
    }

    private static final CascadeClassifier faceDetector;

    static Image detectFaces(File f) throws IOException{
        Mat imageMat = Imgcodecs.imread(f.getPath());
        List<Rect> faces = detectFacesRects(imageMat);

        for (Rect rect : faces) {
            Imgproc.rectangle(imageMat, new Point(rect.x, rect.y), new Point(rect.x + rect.width,
                    rect.y + rect.height), new Scalar(0, 255, 0), 10);
        }
        return ConvertToImage(imageMat);
    }


    static List<Rect> detectFacesRects(Mat imageMat) {
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(imageMat, faceDetections, 1.2, 3,
                0, new Size(75, 75), new Size(1000,1000));
        return Arrays.asList(faceDetections.toArray());
    }

    private static Image ConvertToImage(Mat m) throws IOException {
        int type = BufferedImage.TYPE_BYTE_GRAY;

        if ( m.channels() > 1 ) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels()*m.cols()*m.rows();
        byte [] b = new byte[bufferSize];
        m.get(0,0,b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(),m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return SwingFXUtils.toFXImage(image, null);
    }
}
