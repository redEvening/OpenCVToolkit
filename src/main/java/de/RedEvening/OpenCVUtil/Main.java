package de.RedEvening.OpenCVUtil;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

import static org.opencv.core.Core.*;

public class Main extends Application {

    static{
        nu.pattern.OpenCV.loadShared();
        System.loadLibrary(NATIVE_LIBRARY_NAME);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view.fxml"));

        Parent root = loader.load();
        primaryStage.setTitle("Face Detector");
        primaryStage.setScene(new Scene(root, 800,500));
        ((Controller) loader.getController()).setupBindings(primaryStage.getScene().getWindow());
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
