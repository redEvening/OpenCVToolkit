package de.RedEvening.OpenCVUtil;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.imgcodecs.Imgcodecs;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

final class ShuffleFacesUtil {

    private ShuffleFacesUtil(){}

    static Image shuffleFaces(File f) throws IOException {
        final long seed = System.nanoTime();
        Mat imageMat = Imgcodecs.imread(f.getPath());
        List<Rect> facesList = DetectFacesUtil.detectFacesRects(imageMat);
        Collections.shuffle(facesList, new Random(seed));
        BufferedImage bufferedImage = ImageIO.read(f);

        // Cut subImages,clone and store them
        List<BufferedImage> imgBuffer = facesList.stream().map(
                rect -> deepcloneImage(
                        bufferedImage.getSubimage(rect.x, rect.y, rect.width, rect.height)))
                .collect(Collectors.toList());
        Collections.shuffle(imgBuffer, new Random(seed));


        for (int i = 0; i < Math.ceil((double) imgBuffer.size() / 2); i++){
            final int swap = imgBuffer.size() - 1 - i;
            Point posFirst = new Point(facesList.get(i).x, facesList.get(i).y);
            Point posSecond = new Point(facesList.get(swap).x, facesList.get(swap).y);
            BufferedImage first = imgBuffer.get(i);
            BufferedImage second = imgBuffer.get(swap);

            swapFaces(bufferedImage, first, posFirst, second, posSecond);
        }
        return SwingFXUtils.toFXImage(bufferedImage, null);
    }

    private static void swapFaces(BufferedImage imageToWriteTo, BufferedImage first, Point posFirst,
                           BufferedImage second, Point2D posSecond) throws IOException {
        Graphics2D canvas = imageToWriteTo.createGraphics();

        // Resize Images to be as big as the other one
        Dimension firstDimension = new Dimension(first.getWidth(), first.getHeight());
        first = scaleImage(first, second.getWidth(), second.getHeight());
        second = scaleImage(second, (int) firstDimension.getWidth(), (int) firstDimension.getHeight());

        // Swap faces
        canvas.drawImage(first, null, (int) posFirst.getX(),(int) posFirst.getY());
        canvas.drawImage(second, null, (int) posSecond.getX(), (int) posSecond.getY());
        canvas.dispose();
    }

    private static BufferedImage deepcloneImage(BufferedImage image){
        BufferedImage clone = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        Graphics2D g = clone.createGraphics();
        g.drawImage(image, null, 0, 0 );
        g.dispose();
        return clone;
    }

    private static BufferedImage scaleImage(BufferedImage image, int width, int height) throws IOException {
        int imageWidth  = image.getWidth();
        int imageHeight = image.getHeight();

        double scaleX = (double) width / imageWidth;
        double scaleY = (double) height / imageHeight;
        AffineTransform scaleTransform = AffineTransform.getScaleInstance(scaleX, scaleY);
        AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform, AffineTransformOp.TYPE_BILINEAR);

        return bilinearScaleOp.filter(image, new BufferedImage(width, height, image.getType()));
    }
}
