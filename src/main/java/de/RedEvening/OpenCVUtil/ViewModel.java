package de.RedEvening.OpenCVUtil;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.image.Image;

import java.io.File;

public class ViewModel {

    private File currentFile;

    private BooleanProperty fileIsOpenedProperty = new SimpleBooleanProperty();
    private ObjectProperty<Image> imageProperty = new SimpleObjectProperty<>();

    public ObjectProperty<Image> getCurrentImgProperty(){
        return imageProperty;
    }

    public File getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(File f) {
        currentFile = f;
        fileIsOpenedProperty.set(f != null);
    }

    public BooleanProperty getFileIsOpenedProperty() {
        return fileIsOpenedProperty;
    }
}